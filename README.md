# inventario
Practica Final VUE

nombre: Eddy Eufracio Quelca Tancara
CI: 4829739


## backend

 json-server backend/db.json

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
